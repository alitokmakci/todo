const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'class',
  content: [
    "./index.html",
    "./src/**/*.vue",
    "./src/**/**/*.vue",
  ],
  theme: {
    extend: {
    },
  },
  plugins: [],
}
