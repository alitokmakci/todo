import axios from 'axios'
import {Storage} from '@capacitor/storage'

const instance = axios.create({
	baseURL: 'https://gnb-todo.herokuapp.com/'
	//baseURL: 'http://localhost:8000'
})

async function getToken() {
	return JSON.parse((await Storage.get({key: 'token'})).value)
}

instance.interceptors.request.use(async (config) => {
	let token = await getToken()
	config.headers.Authorization = `Bearer ${token}`

	return config
})


export default instance
