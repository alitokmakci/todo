const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue'),
    },
    {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "register" */ '../views/Register.vue')
    },
    {
        path: '/list/:default/:index',
        name: 'list',
        component: () => import(/* webpackChunkName: "list" */ '../views/List.vue')
    },
    {
        path: '/task/:task',
        name: 'task',
        component: () => import(/* webpackChunkName: "task" */ '../views/Task.vue')
    }
]

export default routes
