import { createRouter, createWebHistory } from 'vue-router'
import routes from './routes'
import { Storage } from '@capacitor/storage'

async function getToken() {
    return JSON.parse((await Storage.get({key: 'token'})).value)
}

const router = createRouter({
    history: createWebHistory(),
    routes
})

router.beforeEach(async (to, from, next) => {
    let token = await getToken()

    return new Promise(resolve => {
        if (!token) {
            if (to.path !== '/login' && to.path !== '/register') {
                resolve(next({ name: 'login' }))
            } else {
                resolve(next())
            }
        } else {
            if (to.path !== '/login' && to.path !== '/register') {
                resolve(next())
            } else {
                resolve(next({ name: 'home' }))
            }
        }
    })
})

export default router
