import {createStore} from 'vuex'
import axios from '../lib/axios'
import {Dialog} from '@capacitor/dialog'

const store = createStore({
	state() {
		return {
			user: null,
			error: null,
			defaultLists: [],
			customLists: [],
			activeTask: null
		}
	},
	mutations: {
		setUser(state, user) {
			state.user = user
		},
		setError(state, error) {
			state.error = error
		},
		clearError(state) {
			state.error = null
		},
		setDefaultLists(state, lists) {
			state.defaultLists = lists
		},
		setCustomLists(state, lists) {
			state.customLists = lists
		},
		setActiveTask(state, task) {
			return new Promise(resolve => {
				state.activeTask = task
				resolve(true)
			})
		}
	},
	getters: {
		isErrorActive(state) {
			return state.error !== null
		}
	},
	actions: {
		async getUser(context) {
			try {
				const response = await axios.get('/me')
				context.commit('setUser', response.data)
			} catch (e) {
				await Dialog.alert({
					title: 'Hata!',
					message: e.response.data.message
				})
			}
		},
		getDefaultLists(context) {
			axios.get('/default-projects').then(response => {
				context.commit('setDefaultLists', response.data)
			}).catch(async (e) => {
				await Dialog.alert({
					title: 'Hata!',
					message: e.response.data.message
				})
			})
		},
		getCustomLists(context) {
			axios.get('/projects').then(response => {
				context.commit('setCustomLists', response.data)
			}).catch(async (e) => {
				await Dialog.alert({
					title: 'Hata!',
					message: e.response.data.message
				})
			})
		}
	}
})

export default store
