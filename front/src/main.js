import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import vClickOutside from "click-outside-vue3"
import axios from './lib/axios'
import store from './store'
import { SplashScreen } from '@capacitor/splash-screen';


const callSplash = async () => {
	await SplashScreen.show({
		showDuration: 2000,
		autoHide: true
	});
}

callSplash()

const app = createApp(App)

app.config.globalProperties.$axios = axios

app.use(vClickOutside)
app.use(router)
app.use(store)
app.mount('#app')
