<?php

namespace App\Models;

class Project extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = ['name', 'color', 'user_id', 'is_default', 'icon'];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
