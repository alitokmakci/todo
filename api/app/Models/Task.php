<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class Task extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = ['name', 'until_at', 'project_id', 'is_done', 'description'];
}
