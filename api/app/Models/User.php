<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    public function tasks()
    {
        return $this->hasManyThrough(Task::class, Project::class);
    }

    public function defaultProjects()
    {
        $defaults = $this->projects()->with('tasks')->where('is_default', true)->orderBy('created_at', 'DESC')->get();

        $todaysProject = new Project([
            'name' => 'Günüm',
            'is_default' => true,
            'color' => 'green-500',
            'icon' => 'ri-sun-line'
        ]);

        $todaysProject->tasks = $this->todaysTasks();

        $delayedProject = new Project([
            'name' => 'Gecikenler',
            'is_default' => true,
            'color' => 'red-500',
            'icon' => 'ri-emotion-sad-line'
        ]);

        $delayedProject->tasks = $this->delayedTasks();

        return collect([$todaysProject, $delayedProject])->merge($defaults);
    }

    public function todaysTasks()
    {
        return $this->tasks()
            ->whereDate('until_at', Carbon::today())
            ->get();
    }

    public function delayedTasks()
    {
        return $this->tasks()
            ->whereDate('until_at', '<', Carbon::today())
            ->get();
    }

    public function customProjects()
    {
        return $this->projects()->with('tasks')->where('is_default', false)->get();
    }
}
