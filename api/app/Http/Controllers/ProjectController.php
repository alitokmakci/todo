<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        return response($request->user()->customProjects());
    }

    public function show()
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ], [
            'required' => ':attribute alanı boş bırakılamaz.',
        ], [
            'name' => 'Liste Adı',
        ]);

        $project = Project::create([
            'name' => $request->input('name'),
            'color' => $request->input('color'),
            'user_id' => $request->user()->id
        ]);

        if (!$project->save()) {
            return response(['error' => 'Server Hatası!'], 500);
        }

        return response($project);
    }

    public function update(Request $request, $project)
    {
        $project = Project::find($project);

        if (!$request->user()->projects->contains($project)) {
            return response('İzinsiz işlem!', 401);
        }

        $data = $this->validate($request, [
            'name' => 'sometimes',
            'color' => 'sometimes'
        ]);

        $result = $project->update($data);

        return response(['updated' => $result]);
    }

    public function destroy(Request $request, $project)
    {
        $project = Project::find($project);

        if (!$request->user()->projects->contains($project)) {
            return response('İzinsiz işlem!', 401);
        }

        $result = $project->delete();

        return response(['deleted' => $result]);
    }
}
