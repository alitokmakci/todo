<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Http\ResponseFactory;

class AuthenticationController extends Controller
{
    public function me(Request $request)
    {
        return response($request->user());
    }

    /**
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'required' => ':attribute alanı boş bırakılamaz.',
            'email.email' => 'Lütfen geçerli bir e-posta adresi giriniz.',
        ], [
            'email' => 'E-Posta',
            'password' => 'Şifre'
        ]);

        $user = User::where('email', $request->input('email'))->first();

        if (!$user) {
            return response(['error' => 'Bu E-posta adresi kayıtlı değil.'], 401);
        }

        if (Hash::check($request->input('password'), $user->password)) {
            return response($user);
        } else {
            return response(['error' => 'E-posta adresi ve şifre eşleşmiyor.'], 401);
        }
    }

    /**
     * @param Request $request
     * @return Response|ResponseFactory
     * @throws ValidationException
     */
    public function register(Request $request)
    {
        asdasd;

        $credentials = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6'
        ], [
            'required' => ':attribute alanı boş bırakılamaz.',
            'email.unique' => 'Bu e-posta adresi zaten kullanımda.',
            'email.email' => 'Lütfen geçerli bir e-posta adresi giriniz.',
            'password.min' => 'Şifre alanı minimum :min karakter içermelidir.'
        ], [
            'name' => 'İsim',
            'email' => 'E-Posta',
            'password' => 'Şifre'
        ]);

        $credentials['password'] = Hash::make($credentials['password']);

        $user = new User($credentials);

        $user->token = $this->generateToken();

        if (!$user->save()) {
            return response(['error' => 'Server Hatası!'], 500);
        }

        $this->createDefaultProjectsForUser($user);

        return response($user);
    }

    /**
     * @return string
     */
    protected function generateToken()
    {
        return encrypt(Str::random(20));
    }

    protected function createDefaultProjectsForUser(User $user)
    {
        $user->projects()->createMany([
            [
                'name' => 'Alışveriş Listesi',
                'color' => 'yellow-400',
                'is_default' => true,
                'icon' => 'ri-archive-line'
            ],
            [
                'name' => 'Arşiv',
                'color' => 'blue-400',
                'is_default' => true,
                'icon' => 'ri-shopping-basket-line'
            ]
        ]);
    }
}
