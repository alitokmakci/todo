<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function show()
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ], [
            'required' => ':attribute alanı boş bırakılamaz.',
        ], [
            'name' => 'Liste Adı',
        ]);

        if ($request->input('until_at') == "" || $request->input('until_at') == null) {
            $date = null;
        } else {
            $date = Carbon::parse($request->input('until_at'));
        }

        $task = Task::create([
            'name' => $request->input('name'),
            'project_id' => $request->input('project_id'),
            'until_at' => $date
        ]);

        if (!$task->save()) {
            return response(['error' => 'Server Hatası!'], 500);
        }

        return response($task);
    }

    public function update(Request $request, $task)
    {
        $task = Task::find($task);

        if (!$request->user()->tasks->contains($task)) {
            return response('İzinsiz işlem!', 401);
        }

        $data = $this->validate($request, [
            'name' => 'sometimes',
            'until_at' => 'sometimes',
            'is_done' => 'sometimes',
            'description' => 'sometimes'
        ]);

        $result = $task->update($data);

        return response(['updated' => $result]);
    }

    public function destroy(Request $request, $task)
    {
        $task = Task::find($task);

        if (!$request->user()->tasks->contains($task)) {
            return response('İzinsiz işlem!', 401);
        }

        $result = $task->delete();

        return response(['deleted' => $result]);
    }
}
