<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultProjectController extends Controller
{
    public function index(Request $request)
    {
        $user = $request->user();

        return response($user->defaultProjects());
    }
}
