<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register', 'AuthenticationController@register');
$router->post('/login', 'AuthenticationController@login');
$router->get('/me', 'AuthenticationController@me');

$router->get('/projects', 'ProjectController@index');
$router->post('/projects', 'ProjectController@store');
$router->get('/projects/{project}', 'ProjectController@show');
$router->put('/projects/{project}', 'ProjectController@update');
$router->delete('/projects/{project}', 'ProjectController@destroy');

$router->get('/default-projects', 'DefaultProjectController@index');

$router->post('/tasks', 'TaskController@store');
$router->put('/tasks/{task}', 'TaskController@update');
$router->delete('/tasks/{task}', 'TaskController@destroy');
